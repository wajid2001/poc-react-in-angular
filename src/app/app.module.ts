import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { MyComponentWrapperComponent } from "src/components/my-react-component/MyReactComponentWrapper";
import { POCQrCodeScannerWrapper } from "src/components/POC-Components/QrCodeWrapper";
import { POCButtonWrapper } from "src/components/POC-Components/ButtonWrapper";

@NgModule({
  declarations: [
    AppComponent,
    MyComponentWrapperComponent,
    POCQrCodeScannerWrapper,
    POCButtonWrapper,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
