import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import { QrCodeScanner, Button } from "poc-components-qr-code";
import * as React from "react";

import * as ReactDOM from "react-dom";
import "poc-components-qr-code/dist/index.css";

const containerElementName = "ButtonContainer";

@Component({
  selector: "poc-button",
  template: `<span #${containerElementName}></span>`,
  //   styleUrls: ["../../node_modules/poc-components-qr-code/index.css"],
  encapsulation: ViewEncapsulation.None,
})
export class POCButtonWrapper implements OnChanges, OnDestroy, AfterViewInit {
  @ViewChild(containerElementName, { static: false }) containerRef: ElementRef;

  constructor() {
    this.handleDivClicked = this.handleDivClicked.bind(this);
  }

  public handleDivClicked() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.render();
  }

  ngAfterViewInit() {
    this.render();
  }

  ngOnDestroy() {
    ReactDOM.unmountComponentAtNode(this.containerRef.nativeElement);
  }

  private render() {
    ReactDOM.render(
      <Button>This is Button</Button>,
      this.containerRef.nativeElement
    );
  }
}
